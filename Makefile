CC = arm-none-linux-gnueabi-gcc  #交叉编译工具链
EXEC = hello    			#将要执行的文件
OBJS = hello.o      		#目标文件
CFLAGS += -static  		 #用于c编译器的选项,CXXFLAGS用于c++编译器的选项

all: $(EXEC)
$(EXEC): $(OBJS)
	$(CC) $(LDFLAGS) -o $@ $(OBJS)  #LDFLAGS gcc编译器会用到的一些优化参数

clean:
	-rm -f $(EXEC) *.elf *.gdb *.o

	#参考 http://www.cnblogs.com/taskiller/archive/2012/12/14/2817650.html
