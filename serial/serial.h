#ifndef __SERIAL_H__
#define __SERIAL_H__

//#include <sys/signal.h>
//#include <pthread.h>

//设置波特率

#define BAUDRATE B115200
#define uchar unsigned char 


static int fd;
static uchar serial_r[32];
static uchar serial_t[32];

//定义两个全局变量的数组，用来接收和发送数据
//int serial_r[32];
//int serial_t[32];
//初始化数组
//void init_array(uchar *serial_r,uchar *serial_t);

//打开串口，参数为int型，无返回值，错误检查在函数里面

void open_serial(void);

//设置串口
void set_serial_start(int fd);
//从串口接收数据
void receive_serial(int fd);
//往串口发送数据
int send_serial(int fd);

//关闭串口
void close_serial(int fd);
void set_serial_end(int fd);
//简单的校验功能，用来判断数据的发送和接收是否正确，判断方法为将前31个数据进行相加，并将最终计算的结果存入最后一个数据
//uchar check_sum(uchar* serial);

#endif


