#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QWidget>
#include <QSignalMapper>
#include <QList>
#include "Rfidtopo.h"
#include "cliect_td.h"

namespace Ui {
    class MainWidget;
}
typedef signed   char   int8;
typedef unsigned char   uint8;
typedef unsigned char   byte;
typedef signed   short  int16;
typedef unsigned short  uint16;

typedef signed   long   int32;
typedef unsigned long   uint32;
#define BREAK_UINT32( var, ByteNum ) \
          (byte)((uint32)(((var) >>((ByteNum) * 8)) & 0x00FF))

class MainWidget : public QWidget {
    Q_OBJECT
public:
    MainWidget(QWidget *parent = 0);
    ~MainWidget();

public slots:
    void etc_GetId();
public:
    Ui::MainWidget *ui;
    RfidTopo *rf_thread;
    Cliect *cliect_thread;
};

#endif
