/****************************************************************************
** Meta object code from reading C++ file 'cliect_td.h'
**
** Created: Mon Dec 2 17:12:24 2013
**      by: The Qt Meta Object Compiler version 62 (Qt 4.6.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "cliect_td.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'cliect_td.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.6.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_Cliect[] = {

 // content:
       4,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
       8,    7,    7,    7, 0x05,

 // slots: signature, parameters, type, tag, flags
      35,   23,    7,    7, 0x0a,
      57,    7,    7,    7, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_Cliect[] = {
    "Cliect\0\0Rfid_Changed()\0ipaddr,port\0"
    "GetConnect(char*,int)\0Api_Cliect_GetRfidId()\0"
};

const QMetaObject Cliect::staticMetaObject = {
    { &QThread::staticMetaObject, qt_meta_stringdata_Cliect,
      qt_meta_data_Cliect, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &Cliect::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *Cliect::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *Cliect::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Cliect))
        return static_cast<void*>(const_cast< Cliect*>(this));
    return QThread::qt_metacast(_clname);
}

int Cliect::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QThread::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: Rfid_Changed(); break;
        case 1: GetConnect((*reinterpret_cast< char*(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 2: Api_Cliect_GetRfidId(); break;
        default: ;
        }
        _id -= 3;
    }
    return _id;
}

// SIGNAL 0
void Cliect::Rfid_Changed()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}
QT_END_MOC_NAMESPACE
