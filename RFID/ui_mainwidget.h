/********************************************************************************
** Form generated from reading UI file 'mainwidget.ui'
**
** Created: Mon Dec 2 17:12:19 2013
**      by: Qt User Interface Compiler version 4.6.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWIDGET_H
#define UI_MAINWIDGET_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWidget
{
public:
    QLabel *label;
    QLabel *label_2;

    void setupUi(QWidget *MainWidget)
    {
        if (MainWidget->objectName().isEmpty())
            MainWidget->setObjectName(QString::fromUtf8("MainWidget"));
        MainWidget->resize(800, 480);
        MainWidget->setStyleSheet(QString::fromUtf8("#MainWidget{background-image: url(:/images/356.jpg);}"));
        label = new QLabel(MainWidget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(70, 420, 200, 30));
        label->setStyleSheet(QString::fromUtf8("background-color: rgb(85, 255, 0);"));
        label_2 = new QLabel(MainWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(520, 420, 200, 30));
        label_2->setStyleSheet(QString::fromUtf8("background-color: rgb(85, 255, 0);"));

        retranslateUi(MainWidget);

        QMetaObject::connectSlotsByName(MainWidget);
    } // setupUi

    void retranslateUi(QWidget *MainWidget)
    {
        MainWidget->setWindowTitle(QApplication::translate("MainWidget", "Form", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("MainWidget", "\346\202\250\347\232\204\345\215\241\345\217\267\346\230\257\357\274\232", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("MainWidget", "TextLabel", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWidget: public Ui_MainWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWIDGET_H
