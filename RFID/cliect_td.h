#ifndef CLIECT_H
#define CLIECT_H

#include <QWidget>
#include <QProcess>
#include <QThread>
#include <QString>
#include <QStringList>
#include <QMutex>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <sys/ipc.h>
#include <resolv.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <fcntl.h>
#include "api.h"

class Cliect : public QThread
{
   Q_OBJECT
public:
   Cliect();
   ~Cliect();
   void run();
   QMutex mutex;
   struct timeval tv;
   fd_set sds;
   int coorstate;
   void Cliect_RfidId_Process(unsigned int *id);

signals:

   void Rfid_Changed(void);

public slots:
   void GetConnect(char *ipaddr,int port);

   void Api_Cliect_GetRfidId();



};
#endif
