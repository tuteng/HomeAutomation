#include <QDebug>
#include "mainwidget.h"
#include "ui_mainwidget.h"
#include <QDebug>
#include <QWheelEvent>
#include <QFile>
#include <QDateTime>

#include <unistd.h>
#include <math.h>
#include "api.h"

MainWidget::MainWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MainWidget)
{
    ui->setupUi(this);
    QPalette palette1=this->palette();
    palette1.setBrush(QPalette::Window,QBrush(QPixmap(":/images/356.jpg")));
    this->setPalette(palette1);
    this->setAutoFillBackground(true);
    char *ipaddr="192.168.13.15";
    int port=7838;
    cliect_thread = new Cliect();
    cliect_thread->GetConnect(ipaddr,port);
    connect(cliect_thread,SIGNAL(Rfid_Changed()),SLOT(etc_GetId()));
    cliect_thread->start();
    rf_thread = new RfidTopo();
    etc_GetId();
}
MainWidget::~MainWidget()
{
}

void MainWidget::etc_GetId()
{
        char data[5];
        unsigned long int id;
        data[0]=BREAK_UINT32( pid,3 );
        data[1]=BREAK_UINT32( pid,2 );
        data[2]=BREAK_UINT32( pid,1 );
        data[3]=BREAK_UINT32( pid,0 );
        printf("-------------*********etc_GetId************-------------%lu,%d,%d,%d,%d",pid,data[0],data[1],data[2],data[3]);
        id=data[0]+data[1]*256+data[2]*256*256+data[3]*256*256*256;
        ui->label_2->setText((QString::number(id,16)).toUpper());
}

